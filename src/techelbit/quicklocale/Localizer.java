package techelbit.quicklocale;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import techelbit.quicklocale.parser.BasicTranslationFileParser;
import techelbit.quicklocale.parser.TranslationFileParser;

/**
 *
 * @author elbit
 */
public class Localizer {
    
    private String language = "en";
    private String fallbackLanguage = "en";
    private boolean useFallback = true;
    private Map<String, List<TranslationSet>> translations = new HashMap<>();
    private TranslationFileParser parser = new BasicTranslationFileParser();
    
    public String localize(String key) {
        for (TranslationSet tset : translations.getOrDefault(language, new ArrayList<>())) {
            String result = tset.getTranslation(key);
            if (result != null)
                    return result;
        }
        if (useFallback && !language.equals(fallbackLanguage)) {
            for (TranslationSet tset : translations.getOrDefault(fallbackLanguage, new ArrayList<>())) {
            String result = tset.getTranslation(key);
            if (result != null)
                    return result;
            }
        }
        return key;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public TranslationFileParser getParser() {
        return parser;
    }

    public void setParser(TranslationFileParser parser) {
        this.parser = parser;
    }
    
    public Set<String> getAvailableLanguages() {
        return translations.keySet();
    }

    public String getFallbackLanguage() {
        return fallbackLanguage;
    }
    
    public void useFallabck(boolean useFallback) {
        this.useFallback = useFallback;
    }

    public boolean isUseFallback() {
        return useFallback;
    }

    public void setFallbackLanguage(String fallbackLanguage) {
        this.fallbackLanguage = fallbackLanguage;
    }
    
    public void loadTranslations(String lang, TranslationSet set) {
        if (!translations.containsKey(lang)) {
            translations.put(lang, new ArrayList<>());
        }
        List<TranslationSet> tsets = translations.get(lang);
        tsets.add(set);
        translations.replace(lang, tsets);
    }
    
    public void loadTranslations(String path, TranslationFileParser parser) {
        File file = new File(path);
        if (file.isDirectory()) return; // Implement later
        try {
            String fileContent = Files.readString(file.toPath());
            parser.parse(fileContent).forEach((lang, set) -> {
                loadTranslations(lang, set);
            });
        } catch (IOException ex) {
            // Implement later
        }
    }
    
    public void loadTranslations(String path) {
        loadTranslations(path, parser);
    }
    
    public void loadTranslationsFromDirectory(String path, String extension, TranslationFileParser parser) {
        File dir = new File(path);
        if (!dir.isDirectory()) return; // Implement later
        for (File file : dir.listFiles()) {
            if (file.isDirectory()) {
                loadTranslationsFromDirectory(file.getPath(), extension, parser);
                continue;
            }
            if (extension == null || file.getName().endsWith("." + extension)) {
                loadTranslations(file.getPath(), parser);
            }
        }
    }
    
    public void loadTranslationsFromDirectory(String path, String extension) {
        loadTranslationsFromDirectory(path, extension, parser);
    }
    
    public void loadTranslationsFromDirectory(String path) {
        loadTranslationsFromDirectory(path, null, parser);
    }
    
    public void loadTranslationsFromRawFileContent(String rawContent) {
        parser.parse(rawContent).forEach((lang, set) -> {
                loadTranslations(lang, set);
            });
    }
    
    public void unloadLanguage(String language) {
        translations.remove(language);
    }
    
    public void unloadTranslationSet(String language, String name) {
        List<TranslationSet> lang = translations.get(language);
        lang.removeIf(curr -> curr.getName().equals(name));
        translations.replace(language, lang);
    }
    
    public void unloadTranslationSet(String name) {
        translations.forEach((language, setList)->{
            List<TranslationSet> lang = translations.get(language);
            lang.removeIf(curr -> curr.getName().equals(name));
            translations.replace(language, setList);
        });
    }
    
}
