package techelbit.quicklocale.parser;

import java.util.HashMap;
import java.util.Map;
import techelbit.quicklocale.TranslationSet;

/**
 *
 * @author elbit
 */
public class BasicTranslationFileParser implements TranslationFileParser {

    @Override
    public Map<String, TranslationSet> parse(String fileContent) {
        Map<String, TranslationSet> output = new HashMap<>();
        TranslationSet tset = null;
        Map<String, String> translations = new HashMap<>();
        String lang = "en";
        for (String line : fileContent.split("\n")) {
            if (line.startsWith("#")) continue;
            if (line.startsWith("@end") && tset != null && !translations.keySet().isEmpty()) {
                tset.addTranslations(translations);
                output.put(lang, tset);
                tset = null;
                lang = "ENGLISH";
                continue;
            }
            if (line.startsWith("!")) {
                lang = line.substring(1);
                continue;
            }
            if (line.startsWith("*")) {
                tset = new TranslationSet(line.substring(1));
                continue;
            }
            line = stripSpaces(line);
            if (!line.matches("[\"|\']([ ]*+[0-9A-Za-z]++[ ]*+)+[\"|\']=[\"|\']([ ]*+[0-9A-Za-z]++[ ]*+)+[\"|\']")) continue;
            line = line.replace("\"", "");
            String[] splitLine = line.split("=");
            translations.put(splitLine[0], splitLine[1]);
        }
        if (tset != null && !translations.keySet().isEmpty()) {
            tset.addTranslations(translations);
            output.put(lang, tset);
        }
        return output;
    }
    
    private String stripSpaces(String text) {
        String output = "";
        boolean inQuotes = false;
        for (char l : text.toCharArray()) {
            if (l == '\"' || l == '\'') {
                inQuotes = !inQuotes;
                output += l;
            } else {
                if (l == ' ' && !inQuotes) continue;
                output += l;
            }
        }
        return output;
    }
    
}
