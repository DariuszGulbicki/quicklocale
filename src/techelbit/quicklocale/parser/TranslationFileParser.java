package techelbit.quicklocale.parser;

import java.util.Map;
import techelbit.quicklocale.TranslationSet;

/**
 *
 * @author elbit
 */
public interface TranslationFileParser {
    
    public Map<String, TranslationSet> parse(String fileContent);
    
}
