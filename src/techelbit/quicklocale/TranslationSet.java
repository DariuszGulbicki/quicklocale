package techelbit.quicklocale;

import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author elbit
 */
public class TranslationSet {
    
    private String name;
    private Map<String, String> translations = new HashMap<>();

    public TranslationSet(String name) {
        this.name = name;
    }
    
    public TranslationSet(String name, Map<String, String> translations) {
        this.name = name;
        this.translations = translations;
    }
    
    public String getTranslation(String key) {
        return translations.get(key);
    }

    public String getName() {
        return name;
    }
    
    public void addTranslation(String key, String translation) {
        translations.putIfAbsent(key, translation);
    }
    
    public void addTranslations(Map<String, String> translations) {
        this.translations.putAll(translations);
    }
    
}
